function make_string(height) {
	let string = document.createElement("div");
	string.classList.add("string");
	for(let i = 0; i < height; i++) {
		string.appendChild(document.createElement("span"));
	}
	return string;
}
function make_empty_chord(strings, height) {
	let container = document.createElement("div");
	container.classList.add("chordtab");
	for(let i = 0; i < strings; i++) {
		container.appendChild(make_string(height));
	}
	return container;
}
function fill_chord(el, chord) {
	el.className = "chordtab";
	for(let i = 0; i < chord.length; i++) {
		el.classList.add(`s${i}-${chord[i]}`);
	}
}
